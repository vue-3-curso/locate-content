export const messages={
    en:{
        message:{
            hello:'Hello {name}',
            content:'We are using i18n package',
            products:'no products | one product | {count} products',
            comments:'no comments | comment | comments'
        }
    },
    es:{
        message:{
            hello:'Hola {name}',
            content:'Estamos usando el paquete i18n',
            products:'sin productos | un producto | {count} productos',
            comments:'sin comentarios | comentario | comentario'
        }
    }
}